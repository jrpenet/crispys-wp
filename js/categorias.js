let temp = []
let pedidos = []
let clientes = []

const loadpedidos = function(){
    const pedidosJSON = sessionStorage.getItem('pedidos')
    
    if(pedidosJSON !== null){
        return JSON.parse(pedidosJSON)
    } else {
        return []
    }
}

const savetemp = function(){
    sessionStorage.setItem('temp', JSON.stringify(temp))
}

const savepedidos = function(){
    sessionStorage.setItem('pedidos', JSON.stringify(pedidos))
}

const saveclientes = function(){
    localStorage.setItem('clientes', JSON.stringify(clientes))
}

const btnPedir = document.querySelectorAll('.pedir-prod')
const modalProd = document.querySelector('#modal-produto')
const fechaModal = document.querySelector('.fechar-modal')

const inserePedidos = document.querySelector('#inserePedido')

btnPedir.forEach((botao) => {

    //info dos itens
    const nomeProd = botao.parentNode.childNodes[1].textContent
    const imgProd = botao.parentNode.childNodes[3].src
    const precoProd = parseFloat(botao.parentNode.childNodes[9].textContent.replace("R$ ",'').replace(',', '.'))

    botao.addEventListener('click', (e) => {
        e.preventDefault()
        
        modalProd.setAttribute('class', 'modal-container mostrar')

        temp.push({
            item: nomeProd,
            linkImg: imgProd,
            quantidade: parseInt(botao.parentNode.childNodes[7].value),
            preco: precoProd,
            sub: precoProd * parseInt(botao.parentNode.childNodes[7].value).toFixed(2)
        })

        savetemp()   //sessionStorage.setItem('temp', JSON.stringify(temp))

        const produtoModal = document.querySelector('#prodModal')
        produtoModal.textContent = nomeProd

        const imgModal = document.querySelector('#imgModal')
        imgModal.src = imgProd

        const descricaoModal = document.querySelector('#descricaoModal')
        descricaoModal.textContent = botao.parentNode.childNodes[5].textContent

    })
})

fechaModal.addEventListener('click', () => {
    modalProd.setAttribute('class', 'modal-container')
    sessionStorage.removeItem('temp')
    location.reload()
})

inserePedidos.addEventListener('click', () => {
    console.log(temp[0])

    let boxes = document.querySelectorAll('.checado')
    let rs = ''

    for(let i = 0; i < boxes.length; i++){
        if(boxes[i].checked === true){
            rs += boxes[i].value + ", "
        }
    }

    let counter = document.querySelectorAll('input[type=checkbox]:checked').length

    const observacao = document.querySelector('#obsTXT').value

    if(rs == ''){
        pedidos.push({
            item: temp[0].item + ' Obs.: ' + observacao,
            qtd: temp[0].quantidade,
            subtotal: temp[0].sub + (counter * 2 * temp[0].quantidade)
        })
        sessionStorage.setItem('pedidos', JSON.stringify(pedidos))
    }else{
        pedidos.push({
            item: temp[0].item + ' Molho: ' + rs + ' Obs.: ' + observacao,
            qtd: temp[0].quantidade,
            subtotal: temp[0].sub + (counter * 2 * temp[0].quantidade)
        })
        sessionStorage.setItem('pedidos', JSON.stringify(pedidos))
    }

    modalProd.setAttribute('class', 'modal-container')
    sessionStorage.removeItem('temp')

    location.reload()
})

pedidos = loadpedidos()

const listaPedidoSection = document.querySelector('#sectionPedidosCliente')

if(pedidos.length > 0){
    pedidos.forEach((pedido) => {
        infoContainer = document.createElement('div')
        listaPedidoSection.appendChild(infoContainer)

        infoPedido = document.createElement('p')
        infoPedido.textContent = `${pedido.qtd}x ${pedido.item}`
        infoContainer.appendChild(infoPedido)

        btnEdt = document.createElement('span')
        btnEdt.setAttribute('class', 'material-icons removePed')
        btnEdt.setAttribute('style', 'color: red; cursor: pointer;')
        btnEdt.textContent = 'delete'
        infoContainer.append(btnEdt)
    })

    const showSacolast = document.querySelector('#sacola-st')
        const mostraSub = pedidos.map((e) => e.subtotal).reduce((a, b) => {
            return a + b
        }, 0).toFixed(2).replace('.', ',')

        showSacolast.textContent = `R$ ${mostraSub}`

    const btnRmvPed = document.querySelectorAll('.removePed') //botao de apagar pedido sacola
    
    btnRmvPed.forEach((btnRmv) => {
        btnRmv.addEventListener('click', (e) => {
            e.preventDefault()
            let arr = Array.prototype.slice.call(btnRmvPed)  
            arr.indexOf(btnRmv)
            console.log(arr.indexOf(btnRmv))

            pedidos.splice(arr.indexOf(btnRmv), 1)
            sessionStorage.setItem('pedidos', JSON.stringify(pedidos))

            const showSacolast = document.querySelector('#sacola-st')
            const mostraSub = pedidos.map((e) => e.subtotal).reduce((a, b) => {
                return a + b
            }, 0).toFixed(2).replace('.', ',')

            showSacolast.textContent = `R$ ${mostraSub}`

            location.reload()
            
        })
    })

}else{
    document.querySelector('#sacola-st').textContent = "R$ 0,00"
    listaPedidoSection.textContent = 'Nenhum pedido'
}

const botaoConfirmaPedido = document.querySelector('#btn-confPedido')
botaoConfirmaPedido.addEventListener('click', (e) => {
    e.preventDefault()
    if(clientes.length < 1){
        const mostraModalCliente = document.querySelector('#modal-cliente')
        mostraModalCliente.setAttribute('class', 'modal-container-cliente mostrar')
        
        /*Codigo para inclusao de endereço automatico*/

        const pegaoCep = document.querySelector('#cep')
        pegaoCep.addEventListener('input', () => {
            if(pegaoCep.value.replace('-', '').length > 7){
                const url = 'https://viacep.com.br/ws/' + pegaoCep.value.replace('-', '') +  '/json/'
                fetch(url)
                .then((r) => r.json())
                .then(data => {
                    if(data.logradouro == undefined || data.bairro == undefined || data.localidade  == undefined || data.uf  == undefined){
                        alert('Endereço Inválido')
                    }else{
                        const rua = document.querySelector('#rua')
                        rua.value = data.logradouro
                    }
                })
            }
        })

        const confDados = document.querySelector('#inserePerfil')

        /*Armazenamento local cliente*/
        const clienteNome = document.querySelector('#nome')
        const clienteTel = document.querySelector('#cel')
        const clienteRua = document.querySelector('#rua')
        const clienteNrCasa = document.querySelector('#nr')
        const clienteComplemento = document.querySelector('#complemento')
        const clienteBairro = document.querySelector('#bairro')
        const clienteObservacao = document.querySelector('#observacaoPedido')

        confDados.addEventListener('click', (e) => {
            e.preventDefault()

            if(clienteNome.value == '' || clienteTel.value == '' || pegaoCep.value.replace('-', '') == '' || clienteRua.value == '' || clienteNrCasa.value == '' || clienteComplemento.value == '' || clienteBairro.value == ''){
                alert('Favor completar os dados')
                
            }else{
                alert('inseriu')

                clientes.push({
                    nome: clienteNome.value,
                    telefone: clienteNome.value,
                    cep: pegaoCep.value.replace('-', ''),
                    endereco: clienteRua.value,
                    numero: clienteNrCasa.value,
                    complemento: clienteComplemento.value,
                    bairro: clienteBairro.value,
                    observacao: clienteObservacao.value
                })

                saveclientes()
            }   

            const mostraModalCliente = document.querySelector('#modal-cliente')
            mostraModalCliente.setAttribute('class', 'modal-container-cliente')

        })

        const fecharModalCliente = document.querySelector('.fechar-modal-cliente')
        fecharModalCliente.addEventListener('click', () => {
            const mostraModalCliente = document.querySelector('#modal-cliente')
            mostraModalCliente.setAttribute('class', 'modal-container-cliente')
        })
    }
})

