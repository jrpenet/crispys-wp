let temp = []

const loadtemp = function(){
    const tempJSON = sessionStorage.getItem('temp')
    
    if(tempJSON !== null){
        return JSON.parse(tempJSON)
    } else {
        return []
    }
}

const savetemp = function(){
    sessionStorage.setItem('temp', JSON.stringify(temp))
}

//expose orders from module
const gettemp = () => temp

const criatemp = (nome, linkImg, qtd, preco) =>{
    
    temp.push({
        item: nome,
        imagem: linkImg,
        qtd,
        preco,
        sub: qtd * preco
    })
    savetemp()
}

const removetemp = (item) => {
    temp.splice(item, 1)
    savetemp()
}

const apagatemp = () => sessionStorage.removeItem('temp')

temp = loadtemp()

export { gettemp, criatemp, savetemp, removetemp, apagatemp }